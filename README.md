# IDS721 Week9 Project

This project is part of the IDS721 course and focuses on building a Streamlit application with HuggingFace model.

## Install Python and Required Toolkits

The goal of this project is to develop a Streamlit application integrating HuggingFace model.
1. Install Python and required libraries and dependencies
2. Install Nvidia CUDA Toolkit to accelerate the model running
![CUDA](img/cuda.png)


## Create Streamlit Application with HuggingFace Pipeline

In this project, I'm using Facebook's `bart-large-cnn` model to perform text summarization tasks.
1. Create `app.py` for the application
1. Use HuggingFace's `pipeline` API to utilize the bart model

```python
from transformers import pipeline

summarizer = pipeline(task="summarization", model="facebook/bart-large-cnn", device=0)
```

2. Use Streamlit to accept user's input and display the result

We can start running our Streamlit application with `streamlit run app.py` now: 

![StreamlitApp](img/app.png)

We can either use local URL or Network URL provided by Streamlit to access the application.To use the application, simply provide some text input and click the `Summarize` button:

![Result](img/result.png)

## Deploy the Application to Streamlit
Streamlit Community Cloud allows us to deploy apps easily. To deploy the application to Streamlit, we have to:

1. Create a Streamlit account and go to the workspace

![Workspace](img/workspace.png)

2. Create a `requirements.txt` for the application to specify the dependencies
3. Add the app to a Github repository
4. Deploy the app in Streamlit's workspace

![Deploy](img/deploy.png)

5. After successful deplotment, check the deployed application
![StreamlitDeployment](img/streamlitdeploy.png)